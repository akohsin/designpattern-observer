package smsstation;

public class Message {
    String tresc;
    String numer;

    public String getTresc() {
        return tresc;
    }

    public String getNumer() {
        return numer;
    }

    public Message(String numer, String tresc){
        this.tresc = tresc;
        this.numer = numer;
    }
}
