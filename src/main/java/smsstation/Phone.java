package smsstation;

import java.util.Observable;
import java.util.Observer;

public class Phone implements Observer {
    String numer;

    public Phone(String numer) {
        this.numer = numer;
    }

    @Override
    public void update(Observable o, Object sms) {
        if (sms instanceof Message) {
            Message s = (Message) sms;
            if (s.getNumer().equals(numer)) {
                System.out.println(numer + " otrzymał wiadomość : " + s.getTresc());
            }
        }


    }
}
