package chatroom;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class ChatUser implements Observer{
    private int id;
    private String nick;
    private List<String> messages;
    private boolean isAdmin;

    public ChatUser(int id, String nick, boolean isAdmin) {
        this.id = id;
        this.nick = nick;
        this.isAdmin = isAdmin;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    @Override
    public void update(Observable o, Object arg) {

    }
}
