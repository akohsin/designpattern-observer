package chatroom;

import java.util.*;

public class ChatRoom extends Observable {
    private Map<Integer, ChatUser> zalogowani = new HashMap<>();
    String roomName;
    List<Integer> listaAdminow = new ArrayList<Integer>(Arrays.asList(1,2,3,4));
    private int licznik =0;

    public ChatRoom(String roomName) {
        this.roomName = roomName;
        userLogin("admin");
    }

    public void userLogin(String nick){
        if (nick.equals("admin")) {
            ChatUser tmp = new ChatUser(licznik, nick, true);
            zalogowani.put(++licznik, tmp);
            addObserver(tmp);
        }
        else {
            zalogowani.put(++licznik, new ChatUser(licznik, nick, true));
        }
    }
    public void kickUser(int kickowany, int kickujacy){
        if (zalogowani.get(kickujacy).isAdmin()){
            if (kickujacy!=kickowany){
                zalogowani.remove(kickowany);
            }
        }
    }
    public void sendMessage(String nick, String message){
        setChanged();
        notifyObservers();
    }
    //    void sendMessage(int user, String message); - rozsyła wiadomości do wszystkich
//            użytkowników



//    - mapę zalogowanych użytkowników (id -> user)
//            - String roomName;
//- z góry zdefiniowaną listę nicków dla userów którzy są adminami. ( stwórz listę i dodaj od
//    razu np. nick "admin" "administrator", jeśli user zaloguje się z takim nickiem to jest adminem)
//    metody:
//
//    void userLogin(String nick) - loguje użytkownika (dodaje go do kolekcji) --- > Dodatkowe,
//    Później(tutaj będzie podpięcie mechanizmu observer-observable)
//    void sendMessage(int user, String message); - rozsyła wiadomości do wszystkich
//            użytkowników
//    void kickUser(int id_kickowanego, int id_kickującego); - weryfikuje czy kickujący jest
//    adminem i wyrzuca kickowanego.

}
