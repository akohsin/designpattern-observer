Observer-Observable
Przykład: https://bitbucket.org/nordeagda2/designpatternobserverobservableexample

Zadanie 1:

Stworzymy aplikację SMS station. Aplikacja ma symulować pracę stacji smsowej.
Stwórz klasę SmsStation. zakładamy że stacja smsowa otrzymuje jakieś zlecenie o wysłaniu
smsa, które musi się wydarzyć poprzez broadcast. Ta klasa powinna mieć metodę

setChanged i notifyObservers, oraz:
void addPhone(Phone p ) - dodaje telefon do stacji.
void sendSms(String numer, String tresc)

Phone jest klasą która otrzymuje smsy. Każdy telefon ma przypisany numer telefonu.
Metoda sendSms tworzy obiekt typu Message, a następnie rozsyła go. Klasa Message
posiada dwa pola (tresc i numer). Obiekt Message jest rozsyłany, a następnie wiadomości
docierają do wszystkich telefonów w pobliżu. Sms po tym jak dotrze do telefonu,
sprawdzamy czy powinien dotrzeć do tego telefonu (sprawdzamy czy numer z wiadomości
jest taki jak numer z telefonu).


Użytkownik z poziomu Main'a wpisuje komendę:
1235346457 jakas tresc smsa
opcjonalnie dodaj komendę
addPhone nr_tel
alternatywnie do dodawania telefonów do stacji z linii poleceń, możesz dodać kilka telefonów
w konstruktorze stacji.
Po otrzymaniu komendy podziel ją przez split na numer (1 słowo komendy) oraz pozostałą
część komendy - to będzie treść smsa.



Zadanie 2 MNIEJ SAMODZIELNIE:
Stwórz aplikacje (model) ChatRoom'u
Stwórz klasę ChatUser która posiada:
private int id;
private String nick;
private List<String> messages;
private boolean isAdmin;

Stwórz klasę chatRoom która posiada:
- mapę zalogowanych użytkowników (id -> user)
- String roomName;
- z góry zdefiniowaną listę nicków dla userów którzy są adminami. ( stwórz listę i dodaj od
razu np. nick "admin" "administrator", jeśli user zaloguje się z takim nickiem to jest adminem)
metody:

void userLogin(String nick) - loguje użytkownika (dodaje go do kolekcji) --- > Dodatkowe,
Później(tutaj będzie podpięcie mechanizmu observer-observable)
void sendMessage(int user, String message); - rozsyła wiadomości do wszystkich
użytkowników
void kickUser(int id_kickowanego, int id_kickującego); - weryfikuje czy kickujący jest
adminem i wyrzuca kickowanego.

Spróbuj wykonać aplikację tak, aby wykorzystywała mechanizm observer i observable.
Można to zrobić na dwa sposoby:
1. Observable jest server, wysyłamy wiadomość wywołując metodę send a on rozsyła
(notify) (observerem są userzy)
2. Observable i observer to userzy. Wysyłając wiadomość wszyscy są informowani o
rozesłaniu -> UWAGA! każdy użytkownik jest Observerem oraz Observable.
https://bitbucket.org/nordeagda2/designpatternobserverobservablechatroom
Zadanie 3 - Singleton+Observer/Observable:
Stworzymy imitację aplikacji webowej.
Zakładamy istnienie systemu dużej korporacji. Zaprojektuj aplikację która posiada klasę
Database, która służy do zapisywania i odczytywania danych z pliku. Stwórz enuma
DatabaseName który posiada trzy wartości DB_USERS (NIE PRZECHOWUJE SAMYCH
UŻYTKOWNIKÓW, TYLKO KOLEKCJĘ JEGO ZGŁOSZEŃ SERWISOWYCH) ,
DB_ORDER, DB_REQUESTS. Klasa ma posiadać funkcję dopisywania rekordów do bazy
danych oraz ich czytania (musi mieć metody, które przyjmują w/w enuma i na podstawie
tego, jaki enum został przekazany, mają czytać/zapisywać inny plik).
- Stwórz klasę Request - zlecenie które podaje użytkownik. Są różne typy requestów (można
je podzielić przez dziedziczenie lub enuma).
- Stwórz klasę Order - to zamówienie. Na pewien typ Requestu (ktoś zgłasza do naszej firmy
request żeby kupić sprzęt) wytwarza się order i jest logowany do bazy danych.
- Stwórz dodatkowo klasę Marketing, która rozpatruje requesty, ale tylko te, które są typu
zamówieniowego (ktoś chce kupić sprzęt). Po otrzymaniu takiego Requestu marketing
tworzy order oraz umieszcza go w bazie danych (+ wypisuje na ekran).
- Stwórz klasę ServiceDepartment która rozpatruje requesty, ale tylko te, które są typu
serwisowego. Ludzie którzy zgłaszają że coś nie działa. Ta klasa po rozpatrzeniu robi sout
danego requestu oraz dopisuje do bazy danych do db_users (NIE PRZECHOWUJE
SAMYCH UŻYTKOWNIKÓW, TYLKO KOLEKCJĘ JEGO ZGŁOSZEŃ SERWISOWYCH)
wpis o tym jaki użytkownik dokonał zgłoszenia serwisowego. Jeden użytkownik może mieć
więcej niż jedno zgłoszenie serwisowe.
- Stwórz klasę FinanceDepartment - który rozpatruje wszystkie zgłoszenia i po otrzymaniu
zgłoszeń robi i utrzymuje statystykę. Posiada również metodę getStatistics który zwraca
obiekt klasy statistics z wyliczeniem typów zgłoszeń jakie zostały przyjęte.
- Stwórz klasę WebService który jest Observable. On przyjmuje zgłoszenia (ze skanera
wywołujemy jego metody zgłaszania requestów). Po otrzymaniu zgłoszenia rozsyła je
modelem Observer-Observable do klas Marketing, ServiceDepartment, FinanceDepartment.
Instancje tych klas tworzą się w konstruktorze i w tym miejscu inicjalizuje się dodanie
observer/observable. Każdy request po dotarciu do WebService z zewnątrz powinien być
zapisywany do DB_REQUESTS.
https://bitbucket.org/nordeagda2/designpatternobserverobservabledatabase